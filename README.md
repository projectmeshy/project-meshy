![Meshy Logo](https://projectmeshy.gitlab.io/icons/android-icon-144x144.png)

    
# Meshy App

Meshy is a cross platform application built using Flutter that gives 3D object mesh from the Single 2D input image by using GraphCNN and Graph-ResNet.



[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](LICENSE)

## Features

- Runs on Android, Web, Windows and Linux. (iOS and macOS support will be added)
- Live previews and Live rendering of Wavefront output.
- Fullscreen mode
- Download in **.stl**, **.obj**, **.ply**, **.msh**, **.glb**, **.gltf** and **.inp** file formats
- Share as an attachment

  
## Tech Stack

**Client:** Flutter

**Deep Learning Model** Tensorflow v1.3, tflearn, CUDA

**Server:** Google Colab, FastAPI

**Database** Nhost, EasyDB

  
## Deployment

To deploy this project run

```bash
  jupyter Colab_Meshy.ipnyb 
```

  
## Installation 

For Installing the Android and Windows application,

Check out [Releases](https://gitlab.com/projectmeshy/releases) repo.
    
## Screenshots

![App Screenshot](screenshots/MeshyWin.png)

  
## API Reference

#### Convert 2D image to 3D mesh

```http
  POST /convert/
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `api_key` | `string` | **Required**. Your API key |
| `image`   | `image\png` | **Required** Input 2D image |

#### Convert to .stl file format

```http
  GET /convert/stl
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `token_id`      | `string` | **Required**. Id of object to convert |

#### Convert to .ply file format

```http
  GET /convert/ply
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `token_id`      | `string` | **Required**. Id of object to convert |

#### Convert to .msh file format

```http
  GET /convert/ansys
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `token_id`      | `string` | **Required**. Id of object to convert |

#### Convert to .glb file format

```http
  GET /convert/glb
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `token_id`      | `string` | **Required**. Id of object to convert |

#### Convert to .gltf file format

```http
  GET /convert/gltf
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `token_id`      | `string` | **Required**. Id of object to convert |

#### Convert to .inp file format

```http
  GET /convert/abaqus
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `token_id`      | `string` | **Required**. Id of object to convert |

## Roadmap

- Additional browser support since some Firefox browsers doesn't render mesh correctly.
- Add more 3D standard formats to download output mesh.
- Bring custom images support 
- Adding images captured dirrectly from device camera.
- Add more integrations.

  
## Acknowledgements

 - [Pixel2Mesh](https://github.com/nywang16/Pixel2Mesh)
 - [G-ResNet](https://github.com/jwzhanggy/GResNet)

  
## License

[MIT](LICENSE)

  
## Feedback

If you have any feedback, please reach out to [msm17b034@iiitdm.ac.in](mailto:msm17b034@iiitdm.ac.in)

  
